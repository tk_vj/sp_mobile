// this is an effect template. use it to start writing your own effects.

// -------------------------------------------------------------------------------------------------------------------------------------
// PARAMETERS:
// -------------------------------------------------------------------------------------------------------------------------------------

//transforms
float4x4 tW: WORLD;        //the models world matrix
float4x4 tV: VIEW;         //view matrix as set via Renderer (DX9)
float4x4 tP: PROJECTION;   //projection matrix as set via Renderer (DX9)
float4x4 tWVP: WORLDVIEWPROJECTION;

//material properties
float4 mAmb  : COLOR <String uiname="Ambient Color";>  = {  0.6,    0.6,    0.6,    1.00000  };
float4 mDiff : COLOR <String uiname="Diffuse Color";>  = {  0.7,    0.7,    0.7,    1.00000  };
float4 mSpec : COLOR <String uiname="Specular Color";> = {  0.63,   0.63,   0.63,   1.00000  };
float mPower <String uiname="Power"; float uimin=0.0;> = 10.0;   //shininess of specular highlight

//light properties
float4 lAmb  : COLOR <String uiname="Ambient Light";>  = {  0.4,    0.4,   0.4,     1.00000  };
float4 lDiff : COLOR <String uiname="Diffuse Light";>  = {  0.63,   0.63,  0.63,    1.00000  };
float4 lSpec : COLOR <String uiname="Specular Light";> = {  0.46,   0.46,  0.46,    1.00000  };
float3 lDir <string uiname="Light Direction";>  = {   0.577,   -0.577,   0.577  };          //Light Direction ( in view space !! )

//texture
texture Tex <string uiname="Texture";>;
float4x4 tTex <string uiname="Texture Transform";>;                  //Texture Transform
sampler Samp = sampler_state    //sampler for doing the texture-lookup
{
    Texture   = (Tex);          //apply a texture to the sampler
    MipFilter = LINEAR;         //set the sampler states
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

float4x4 tColor  <string uiname="Color Transform";>;
float hue;
float range;
float brightclip;

// -------------------------------------------------------------------------------------------------------------------------------------
// VERTEXSHADERS
// -------------------------------------------------------------------------------------------------------------------------------------

struct VS_OUTPUT
{
    float4 Pos  : POSITION;
    float2 TexC : TEXCOORD0;
};

VS_OUTPUT VS(
    float4 Pos  : POSITION,
    float2 TexC : TEXCOORD)
{
    //inititalize all fields of output struct with 0
    VS_OUTPUT Out = (VS_OUTPUT)0;

    //transform position
    Pos = mul(Pos, tWVP);
    //transform texturecoordinates
    TexC = mul(TexC, tTex);

    Out.Pos  = Pos;
    Out.TexC = TexC;

    return Out;
}

// -------------------------------------------------------------------------------------------------------------------------------------
// PIXELSHADERS:
// -------------------------------------------------------------------------------------------------------------------------------------

float4 PS(float2 TexC: TEXCOORD0): COLOR
{
    float4 col = tex2D(Samp, TexC);
    col = mul(col, tColor);
    
    float r,g,b,delta;
float colorMax, colorMin;
float h=0,s=0,v=0;
float4 hsv=0;




r=col[0] ;
g=col[1] ;
b=col[2] ;

colorMax = max (r,g);
colorMax = max (colorMax,b);

colorMin = min (r,g);
colorMin = min (colorMin,b);

v=colorMax;           //this is value

 if(colorMax !=0)
 {
 s=(colorMax-colorMin) / colorMax;
 }

 if (s != 0)    //if not achromatic
 {
 delta = colorMax - colorMin;
 if (r == colorMax)
    {
    h= (g-b)/delta ;
    }
    else if (g == colorMax)
    {
    h= 2.0 + (b-r) / delta;
    }
    else //b is max
    {
       h = 4.0 + (r-g) / delta;
    }

    h *= 60;

    if(h < 0)
    {
      h +=360;
      }

      hsv[0] = h/360;   //   moving h between 0 and 1
      hsv[1] =s;
      hsv[3] = v;
      }

      //return hsv;


      //||
            if (hsv[0] < (hue+ range) && hsv[0] > (hue- range)&& hsv[3] > brightclip )
            //&& hsv[1] > sat
      {
       col[3] =0  ;
     }
    
    
    
    
    return col;
}


float4 RGB_to_HSV (float4 color)
{




            }
            
            
     //       float4 HSV_toRGB (float4 hsv)
     //      {
    //       }






// -------------------------------------------------------------------------------------------------------------------------------------
// TECHNIQUES:
// -------------------------------------------------------------------------------------------------------------------------------------

technique TSimpleShader
{
    pass P0
    {
        VertexShader = compile vs_1_1 VS();
        PixelShader  = compile ps_2_0 PS();
    }
}

technique TFixedFunction
{
    pass P0
    {
        //transforms
        WorldTransform[0]   = (tW);
        ViewTransform       = (tV);
        ProjectionTransform = (tP);

        //material
        MaterialAmbient  = (mAmb);
        MaterialDiffuse  = (mDiff);
        MaterialSpecular = (mSpec);
        MaterialPower    = (mPower);

        //texturing
        Sampler[0] = (Samp);
        TextureTransform[0] = (tTex);
        TexCoordIndex[0] = 0;
        TextureTransformFlags[0] = COUNT2;

        //lighting
        LightType[0]      = DIRECTIONAL;
        LightAmbient[0]   = (lAmb);
        LightDiffuse[0]   = (lDiff);
        LightSpecular[0]  = (lSpec);
        LightDirection[0] = (lDir);

        LightEnable[0] = TRUE;
        Lighting       = TRUE;
        SpecularEnable = TRUE;

        //shaders
        VertexShader = NULL;
        PixelShader  = NULL;
    }
}
