float4x4 tW: WORLD;        //the models world matrix
float4x4 tV: VIEW;         //view matrix as set via Renderer (DX9)
float4x4 tP: PROJECTION;   //projection matrix as set via Renderer (DX9)
float4x4 tWVP: WORLDVIEWPROJECTION;



/************* TWEAKABLES **************/

float NumTiles <

    float UIMin = 1.0f;
    float UIMax = 100.0f;
    float UIStep = 1.0f;
> = 50.0;


float DotSize = 1;
float Softness = 0.1;

float Sat <string uiname="Brightness";> = 1;


texture Tex <string uiname="Texture";>;
float4x4 tTex <string uiname="Texture Transform";>;                  //Texture Transform
sampler Samp = sampler_state    //sampler for doing the texture-lookup
{
    Texture   = (Tex);
    AddressU  = WRAP;
    AddressV  = WRAP;          //apply a texture to the sampler
    MipFilter = LINEAR;         //set the sampler states
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

float4x4 tColor  <string uiname="Color Transform";>;

/////////////////////////////////////////


struct vertexOutput {
    float4 HPosition: POSITION;
    float4 UV  : TEXCOORD0;
    float stride: TEXCOORD1;
    float dotsize: TEXCOORD2;
    float halfstride: TEXCOORD3;
};

////////////////////////vertex shader//////////////////////////////

vertexOutput VS_Quad(float4 Position : POSITION,float4 TexCoord : TEXCOORD0)
{
    vertexOutput OUT = (vertexOutput)0;
    OUT.HPosition = mul((float4(Position)),tWVP);
    OUT.UV = mul(TexCoord,tTex);
    OUT.stride = 1.0/NumTiles;
    OUT.halfstride = OUT.stride/2.0;
    OUT.dotsize = DotSize * OUT.stride;

    return OUT;
}

/////////////////// pixel shader /////////////////////////////////////

float4 pixelsPS(vertexOutput IN) : COLOR {
    float4 Pbase = IN.UV - fmod(IN.UV, IN.stride);
    float2 PCenter = Pbase + IN.halfstride;
    float4 tileColor = tex2D(Samp,PCenter);

    float isinDot = length(IN.UV - PCenter) < IN.dotsize / 2;
    return isinDot* tileColor;
}

///////////////////////////////////////////////////////////////////////
float4 halftonePS(vertexOutput IN) : COLOR {
    float2 Pbase = IN.UV - fmod(IN.UV, IN.stride);
    float2 PCenter = Pbase + IN.halfstride;
    float4 tileColor = tex2D(Samp,PCenter);

    float bright = length(tileColor.rgb) / 1.732; // white = (1 + 1 + 1)^.5 = 1.732
    float isinDot = length(IN.UV - PCenter) < bright * IN.dotsize;
    return isinDot*(tileColor*Sat);
}
//////////////////////////////////////////////////////////////

float4 softhalftonePS(vertexOutput IN) : COLOR {
    float2 Pbase = IN.UV - fmod(IN.UV, IN.stride);
    float2 PCenter = Pbase + IN.halfstride;
    float4 tileColor = tex2D(Samp,PCenter);

    float bright = length(tileColor.rgb) / 1.732; // white = (1 + 1 + 1)^.5 = 1.732
    float rad = length(IN.UV - PCenter);
    float s = bright * IN.dotsize;
    float isinDot = rad < s;

    float4 result = .5 - (rad / s - 1) / Softness;
    return result*(tileColor*Sat);
}
////////////////////////////////////////////////////////////////

float4 overlap5halftoneBWPS(vertexOutput IN) : COLOR {
    float2 Pbase = IN.UV - fmod(IN.UV, IN.stride);
    float2 PCenter = Pbase + IN.halfstride;

    float2 CurrentCircle = PCenter + float2(0, -1) * IN.stride;
    float4 CurrentColor = tex2D(Samp,CurrentCircle);
    float bright = CurrentColor.r; //length(CurrentColor.rgb);
    float isinDot = length(IN.UV - CurrentCircle) < bright * IN.dotsize;
    float tileColor = isinDot;

    CurrentCircle = PCenter + float2(0, 1) * IN.stride;
    CurrentColor = tex2D(Samp,CurrentCircle);
    bright = CurrentColor.r ; //length(CurrentColor.rgb);
    isinDot = length(IN.UV - CurrentCircle) < bright * IN.dotsize;
    tileColor = tileColor + isinDot;

    for (int x = -1; x <= 1; x++)
    {
      CurrentCircle = PCenter + float2(x, 0) * IN.stride;
      CurrentColor = tex2D(Samp,CurrentCircle);
      bright = CurrentColor.r ; //length(CurrentColor.rgb);
      isinDot = length(IN.UV - CurrentCircle) < bright * IN.dotsize;
      tileColor = tileColor + isinDot;
    }

    float4 result = tileColor;
    return tileColor*(tileColor*Sat);
}
////////////////////////////////////////////////////////////////
float4 linehalftonePS(vertexOutput IN) : COLOR {
    float2 Pbase = IN.UV - fmod(IN.UV, IN.stride);

    float4 tileColor = tex2D(Samp,Pbase);

    float bright = tileColor.rgb /1.732; // white = (1 + 1 + 1)^.5 = 1.732
    float isinDot = IN.UV - Pbase < bright * IN.dotsize;
    return isinDot;
    }
///////////////////////////////////////////////////////
float4 halftonepictPS(vertexOutput IN) : COLOR {
    float2 Pbase = IN.UV - fmod(IN.UV, IN.stride);
    float4 tile = tex2D(Samp,IN.UV*NumTiles);
    float4 tileColor = tex2D(Samp,Pbase);
    float4 col = (tile * tileColor)*2;

    return col;





}

technique pixels

{
    pass p0

    {		
		VertexShader = compile vs_2_0 VS_Quad();
	
		PixelShader = compile ps_2_0 pixelsPS();
    }
}

technique haftone

{
    pass p0

    {		
		VertexShader = compile vs_2_0 VS_Quad();
	
		PixelShader = compile ps_2_0 halftonePS();
    }
}

technique softhaftone

{
    pass p0

    {		
		VertexShader = compile vs_2_0 VS_Quad();
	
		PixelShader = compile ps_2_0 softhalftonePS();
    }
}

technique overlap5haftoneBW

{
    pass p0

    {		
		VertexShader = compile vs_2_0 VS_Quad();
	
		PixelShader = compile ps_2_0 overlap5halftoneBWPS();
    }
}

 technique linehalftone

{
    pass p0

    {		
		VertexShader = compile vs_2_0 VS_Quad();
	
		PixelShader = compile ps_2_0 linehalftonePS();
    }
}
  technique halftonepic
  {
    pass p0

    {		
		VertexShader = compile vs_2_0 VS_Quad();
	
		PixelShader = compile ps_2_0  halftonepictPS();
    }
}

