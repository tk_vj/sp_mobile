

//transforms
float4x4 tW: WORLD; //the models world matrix
float4x4 tV: VIEW; //view matrix as set via Renderer (DX9)
float4x4 tP: PROJECTION; //projection matrix as set via Renderer (DX9)
float4x4 tWVP: WORLDVIEWPROJECTION;

//texture0
texture Tex <string uiname="LiveStream";>;
sampler Samp = sampler_state //sampler for doing the texture-lookup
{
   Texture   = (Tex);          //apply a texture to the sampler
   MipFilter = LINEAR;         //sampler states
   MinFilter = LINEAR;
   MagFilter = LINEAR;
};


//texture2
texture Tex2 <string uiname="Delayed Image level2";>;
sampler Samp2 = sampler_state //sampler for doing the texture-lookup
{
   Texture   = (Tex2);         //apply a texture to the sampler
   MipFilter = LINEAR;         //sampler states
   MinFilter = LINEAR;
   MagFilter = LINEAR;
};

//texture3
texture Tex3 <string uiname="Delayed Image level3";>;
sampler Samp3 = sampler_state //sampler for doing the texture-lookup
{
   Texture   = (Tex3);         //apply a texture to the sampler
   MipFilter = LINEAR;         //sampler states
   MinFilter = LINEAR;
   MagFilter = LINEAR;
};

//texture4
texture Tex4 <string uiname="Delayed Image level4";>;
sampler Samp4 = sampler_state //sampler for doing the texture-lookup
{
   Texture   = (Tex4);         //apply a texture to the sampler
   MipFilter = LINEAR;         //sampler states
   MinFilter = LINEAR;
   MagFilter = LINEAR;
};

//texture5
texture Tex5 <string uiname="Delayed Image level5";>;
sampler Samp5 = sampler_state //sampler for doing the texture-lookup
{
   Texture   = (Tex5);         //apply a texture to the sampler
   MipFilter = LINEAR;         //sampler states
   MinFilter = LINEAR;
   MagFilter = LINEAR;
};

//texture6
texture Tex6 <string uiname="Delayed Image level6";>;
sampler Samp6 = sampler_state //sampler for doing the texture-lookup
{
   Texture   = (Tex6);         //apply a texture to the sampler
   MipFilter = LINEAR;         //sampler states
   MinFilter = LINEAR;
   MagFilter = LINEAR;
};

//texture7
texture Tex7 <string uiname="Delayed Image level7";>;
sampler Samp7 = sampler_state //sampler for doing the texture-lookup
{
   Texture   = (Tex7);         //apply a texture to the sampler
   MipFilter = LINEAR;         //sampler states
   MinFilter = LINEAR;
   MagFilter = LINEAR;
};

//texture8
texture Tex8 <string uiname="Delayed Image level8";>;
sampler Samp8 = sampler_state //sampler for doing the texture-lookup
{
   Texture   = (Tex8);         //apply a texture to the sampler
   MipFilter = LINEAR;         //sampler states
   MinFilter = LINEAR;
   MagFilter = LINEAR;
};

//texture transformation marked with semantic TEXTUREMATRIX to achieve symmetric transformations
float4x4 tTex: TEXTUREMATRIX <string uiname="Texture Transform";>;




struct vs2ps
{
   float4 Pos : POSITION;
   float4 TexCd : TEXCOORD0;
};
// -------------------------------------------------------------------------
// VERTEXSHADERS
// -------------------------------------------------------------------------

vs2ps VS(
   float4 Pos : POSITION,               // komma,komma,KlammerZU
   float4 TexCd : TEXCOORD0)
{
   //inititalize all fields of output struct with 0
   vs2ps Out = (vs2ps)0;

   //transform position
   Out.Pos = mul(Pos, tWVP);

   //transform texturecoordinates
   Out.TexCd = mul(TexCd, tTex);



   return Out;
}

// -------------------------------------------------------------------------
// PIXELSHADERS:
// -------------------------------------------------------------------------

float4 simpas(vs2ps In): COLOR
{
 float4 col = tex2D(Samp, In.TexCd);
 return col;
}

//////////////////////////////////////////////////////////

float4 shodel(vs2ps In): COLOR
{
 float4 col = tex2D(Samp8, In.TexCd);
 return col;
}
//////////////////////////////////////////////////////////

float4 add(vs2ps In): COLOR
{
 float4 col = tex2D(Samp, In.TexCd);
 float4 col2= tex2D(Samp2, In.TexCd);
 float4 col3= tex2D(Samp3, In.TexCd);
 float4 col4= tex2D(Samp4, In.TexCd);
 float4 col5= tex2D(Samp5, In.TexCd);
 float4 col6= tex2D(Samp6, In.TexCd);
 float4 col7= tex2D(Samp7, In.TexCd);
 float4 col8= tex2D(Samp8, In.TexCd);
          col = (col+col2+col3+col4+col5+col6+col7+col8)/8;

 return col;
}

//////////////////////////////////////////////////////////

float4 LerpEcho(vs2ps In): COLOR
{
 float4 col = tex2D(Samp, In.TexCd);
 float4 col2= tex2D(Samp2, In.TexCd);
 float4 col3= tex2D(Samp3, In.TexCd);
 float4 col4= tex2D(Samp4, In.TexCd);
 float4 col5= tex2D(Samp5, In.TexCd);
 float4 col6= tex2D(Samp6, In.TexCd);
 float4 col7= tex2D(Samp7, In.TexCd);
 float4 col8= tex2D(Samp8, In.TexCd);
 col = lerp(col8,col7,1*abs(distance(col8,col)));
 col = lerp(col,col6 ,1*abs(distance(col7,col)));
 col = lerp(col,col5 ,1*abs(distance(col6,col)));
 col = lerp(col,col4 ,1*abs(distance(col5,col)));
 col = lerp(col,col3 ,1*abs(distance(col4,col)));
 col = lerp(col,col2 ,1*abs(distance(col3,col)));
 col = lerp(col,col  ,1*abs(distance(col2,col)));
return col;
}

////////////////////////////////////////////////////////////

float4 invLerpEcho(vs2ps In): COLOR
{
 float4 col = tex2D(Samp, In.TexCd);
 float4 col2= tex2D(Samp2, In.TexCd);
 float4 col3= tex2D(Samp3, In.TexCd);
 float4 col4= tex2D(Samp4, In.TexCd);
 float4 col5= tex2D(Samp5, In.TexCd);
 float4 col6= tex2D(Samp6, In.TexCd);
 float4 col7= tex2D(Samp7, In.TexCd);
 float4 col8= tex2D(Samp8, In.TexCd);
          col = lerp(col,col2,1*abs(distance(col2,col)));
          col = lerp(col,col3,1*abs(distance(col3,col)));
          col = lerp(col,col4,1*abs(distance(col4,col)));
          col = lerp(col,col5,1*abs(distance(col5,col)));
          col = lerp(col,col6,1*abs(distance(col6,col)));
          col = lerp(col,col7,1*abs(distance(col7,col)));
          col = lerp(col,col8,1*abs(distance(col8,col)));

 return col;
}

//////////////////////////////////////////////////////////

float4 max(vs2ps In): COLOR
{
 float4 col = tex2D(Samp, In.TexCd);
 float4 col2= tex2D(Samp2, In.TexCd);
 float4 col3= tex2D(Samp3, In.TexCd);
 float4 col4= tex2D(Samp4, In.TexCd);
 float4 col5= tex2D(Samp5, In.TexCd);
 float4 col6= tex2D(Samp6, In.TexCd);
 float4 col7= tex2D(Samp7, In.TexCd);
 float4 col8= tex2D(Samp8, In.TexCd);

          col = max(col,col2);
          col = max(col,col3);
          col = max(col,col4);
          col = max(col,col5);
          col = max(col,col6);
          col = max(col,col7);
          col = max(col,col8);
 return col;
}

//////////////////////////////////////////////////////////

float4 min(vs2ps In): COLOR
{
 float4 col = tex2D(Samp, In.TexCd);
 float4 col2= tex2D(Samp2, In.TexCd);
 float4 col3= tex2D(Samp3, In.TexCd);
 float4 col4= tex2D(Samp4, In.TexCd);
 float4 col5= tex2D(Samp5, In.TexCd);
 float4 col6= tex2D(Samp6, In.TexCd);
 float4 col7= tex2D(Samp7, In.TexCd);
 float4 col8= tex2D(Samp8, In.TexCd);

          col = min(col,col2);
          col = min(col,col3);
          col = min(col,col4);
          col = min(col,col5);
          col = min(col,col6);
          col = min(col,col7);
          col = min(col,col8);
 return col;
}

// -------------------------------------------------------------------------
// TECHNIQUES:
// -------------------------------------------------------------------------


technique showLiveStreamOnly
{
   pass P0
   {
       VertexShader = compile vs_1_0 VS();
       PixelShader  = compile ps_1_0 simpas();
   }
}

technique showLastDelayedImageOnly
{
   pass P0
   {
       VertexShader = compile vs_1_0 VS();
       PixelShader  = compile ps_1_0 shodel();
   }
}


technique Addition
{
   pass P0
   {
       VertexShader = compile vs_1_0 VS();
       PixelShader  = compile ps_2_0 add();
   }
}



technique LerpEcho
{
   pass P0
   {
       VertexShader = compile vs_1_0 VS();
       PixelShader  = compile ps_2_0 LerpEcho();
   }
}

technique invLerpEcho
{
   pass P0
   {
       VertexShader = compile vs_1_0 VS();
       PixelShader  = compile ps_2_0 invLerpEcho();
   }
}

technique MaxEcho
{
   pass P0
   {
       VertexShader = compile vs_1_0 VS();
       PixelShader  = compile ps_2_0 max();
   }
}

technique MinEcho
{
   pass P0
   {
       VertexShader = compile vs_1_0 VS();
       PixelShader  = compile ps_2_0 min();
   }
}
