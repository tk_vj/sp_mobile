
// this is an effect template. use it to start writing your own effects.

// -------------------------------------------------------------------------------------------------------------------------------------
// PARAMETERS:
// -------------------------------------------------------------------------------------------------------------------------------------

//transforms
float4x4 tW: WORLD;                  //the models world matrix
float4x4 tV: VIEW;                   //view matrix as set via Renderer (DX9)
float4x4 tP: PROJECTION;             //projection matrix as set via Renderer (DX9)
float4x4 tWVP: WORLDVIEWPROJECTION;  //WORLD*VIEW*PROJECTION


//the vvvv pins are defined here:
texture Tex <string uiname="Texture";>;
float4x4 tTex <string uiname="Texture Transform";>; //Texture Transform
float rOffset  <string uiname="red offset";>;
float gOffset  <string uiname="green offset";>;
float bOffset  <string uiname="blue offset";>;

//this pin definition is more simple:
float lightness;
//the pin name is the name of the variable


sampler Samp = sampler_state    //sampler for doing the texture-lookup
{
    Texture   = (Tex);          //apply a texture to the sampler
    MipFilter = LINEAR;         //set the sampler states
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};


// -------------------------------------------------------------------------------------------------------------------------------------
// VERTEXSHADERS
// -------------------------------------------------------------------------------------------------------------------------------------

struct VS_OUTPUT
{
    float4 Pos  : POSITION;
    float2 TexC : TEXCOORD0;
};


VS_OUTPUT VS(
    float4 Pos  : POSITION,
    float2 TexC : TEXCOORD)
{
    //inititalize all fields of output struct with 0
    VS_OUTPUT Out = (VS_OUTPUT)0;

    //transform vertex position
    Pos = mul(Pos, tWVP);
    
    //transform texturecoordinates
    TexC = mul(TexC, tTex);

    Out.Pos  = Pos;
    Out.TexC = TexC;

    return Out;
}

// -------------------------------------------------------------------------------------------------------------------------------------
// PIXELSHADERS:
// -------------------------------------------------------------------------------------------------------------------------------------

float4 PS(float2 TexC: TEXCOORD0): COLOR
{
    float4 col = tex2D(Samp, TexC);
    
    //change the colorchannels
    
    //red
    col.r = col.r + rOffset + lightness;

    //green
    col.g = col.g + gOffset + lightness;
    
    //blue
    col.b = col.b + bOffset + lightness;
    
    //alpha (the line does nothing)
    col.a = col.a;

    return col;
}

// -------------------------------------------------------------------------------------------------------------------------------------
// TECHNIQUES:
// -------------------------------------------------------------------------------------------------------------------------------------

technique ColorChange
{
    pass P0
    {
        VertexShader = compile vs_1_1 VS();
        PixelShader  = compile ps_1_4 PS();
    }
}

